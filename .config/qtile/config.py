import os
import re
import socket
import subprocess
from libqtile import qtile
from libqtile.config import Click, Drag, Group, KeyChord, Key, Match, Screen, KeyChord
#from libqtile.command import lazy
from libqtile import layout, bar, hook
from libqtile import widget
#from libqtile.widget import StatusNotifier
from libqtile import config
from libqtile.layout import  base
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from typing import List  # noqa: F401
from libqtile.backend.wayland import InputConfig

from qtile_extras import widget
from qtile_extras.widget import StatusNotifier
from qtile_extras.widget.decorations import BorderDecoration
from qtile_extras.widget.decorations import RectDecoration
from qtile_extras.widget.decorations import PowerLineDecoration
from qtile_extras.layout.decorations import RoundedCorners
from qtile_extras.layout.decorations import GradientBorder
from qtile_extras.layout.decorations import CustomBorder

import os
os.environ["XCURSOR_SIZE"] = "24"
os.environ["XDG_CURRENT_DESKTOP"] = "qtile"

home = os.path.expanduser('~') # defining home directory for the hardware configs   

myTerm = ""
drun = ""
runlauncher = ""
bgchange = ""
full_screenshot = ""
screenshot = ""
lockscreen = ""
log_out = ""

if qtile.core.name == "x11":
    myTerm = "kitty"
    runlauncher = "rofi -show run -display-run ' Run: '"
    bgchange = "feh --bg-fill --randomize "+home+"/wallpapers"
    drun = "rofi -show drun -display-drun ' Desktop: '"
    full_screenshot = "xfce4-screenshooter"
    screenshot = "xfce4-screenshooter"
    log_out = home + "/.local/bin/logout-script"
    lockscreen = "dm-logout"
elif qtile.core.name == "wayland":
     myTerm = "foot"
     runlauncher = home + "/.local/bin/my-bemenu -r"
     bgchange = "setrandom "+home+"/wallpapers"
     drun = home + "/.local/bin/my-bemenu -d"
     full_screenshot = "grimshot save output"
     screenshot = "grimshot save area"
     log_out = home + "/.local/bin/logout-script"
     lockscreen = "swaylock -f -i ~/wallpapers/4.jpg --effect-blur 10x5 --clock --indicator"
mod = "mod4"              # Sets mod key to SUPER/WINDOWS

upvol = home + "/.local/bin/volctrl set +5"
downvol = home + "/.local/bin/volctrl set -5"
backlight_up = home + "/.local/bin/blctrl set +5"
backlight_down = home + "/.local/bin/blctrl set -5"
#music = myTerm + " -e cmus"
music = home + "/.local/bin/cmus-control"
fileman = "thunar" #either pcmanfm or "kitty -e ranger"
logout = "dm-logout -d" #either archlinux-logout or "dm-logout -r"
#browser = "flatpak run one.ablaze.floorp"
browser = "flatpak run com.vivaldi.Vivaldi --enable-features=UseOzonePlatform --ozone-platform=wayland"
# browser = "qutebrowser"
browsermenu = home + "/.local/bin/browsermenu"
myEmacs = "emacsclient -c -a 'emacs' " # The Space at the end is essential

sticky_windows = []

@lazy.function
def toggle_sticky_windows(qtile, window=None):
    if window is None:
        window = qtile.current_screen.group.current_window
    if window in sticky_windows:
        sticky_windows.remove(window)
    else:
        sticky_windows.append(window)
    return window

@hook.subscribe.setgroup
def move_sticky_windows():
    for window in sticky_windows:
        window.togroup()
    return

@hook.subscribe.client_killed
def remove_sticky_windows(window):
    if window in sticky_windows:
        sticky_windows.remove(window)

# Below is an example how to make Firefox Picture-in-Picture windows automatically sticky.
# I have a German Firefox and don't know if the 'name' is 'Picture-in-Picture'.
# You can check yourself with `xprop` and then lookup at the line `wm_name`.
#@hook.subscribe.client_managed
#def auto_sticky_windows(window):
#    info = window.info()
#    if (info['wm_class'] == ['Toolkit', 'firefox']
#            and info['name'] == 'Picture-in-Picture'):
#        sticky_windows.append(window)

# def my_toggle_max():
def toggle_max_layout(qtile):
    current_layout = qtile.current_layout.name
    if current_layout == "max":
        qtile.current_group.use_previous_layout()
    else:
        qtile.current_group.setlayout("max")

keys = [
        ### The essentials
        Key([mod], "Return", lazy.spawn(myTerm), desc='Launches My Terminal'),
        Key([mod, "shift"], "Return", lazy.spawn(fileman), desc='Launches File Manager'),
        Key([mod], "p", lazy.spawn(drun), desc='Rofi'),
        #Key([mod], "d", lazy.spawn("dmenu_run"), desc='dmenu'),
        Key([mod, "shift"], "d", lazy.spawn("dm-hub "), desc='dmscripts'),
        Key([mod, "shift"], "m", lazy.spawn(music), desc='music'),
        #Key([mod], "e", lazy.spawn("emacsclient -c -a 'emacs'"), desc='Launch Emacs'),
        Key([mod], "r", lazy.spawn(runlauncher), desc="Run Launcher"),
        Key([mod], "w", lazy.spawn(browser), desc='Open Browser'),
        # Key([mod], "w", lazy.spawn(browsermenu), desc='Open Browsermenu script'),
        # Key([mod, "shift"], "w", lazy.spawn(browser), desc='Open Browser Window'),
        #Key(["mod1"], "x", lazy.spawn("killall autokey-gtk && killall autokey-qt"), desc='End all running macros (autokey)'),
        Key([mod], "v", lazy.spawn(bgchange), desc='random wallpaper'),
        Key([mod, "shift"], "s", lazy.spawn(full_screenshot), desc='Fullscreen Screenshot'),
        Key([mod], "s", lazy.spawn(screenshot), desc='Screenshot'),
        Key([mod], "b", lazy.hide_show_bar(), desc='Toggle Bar'),
        Key([mod, "shift"], "l", lazy.spawn(lockscreen), desc='Lock Screen'),
        Key([mod], "Tab", lazy.next_layout(), desc='Toggle through layouts' ),
        Key([mod, "shift"], "c", lazy.window.kill(), desc='Kill active window'),
        Key([mod, "shift"], "r", lazy.reload_config(), desc='Restart Qtile'),
        Key([mod, "shift"], "q", lazy.spawn(log_out), desc='Log out of Qtile'),
        ### Switch focus of monitors
        Key([mod], "period", lazy.next_screen(), desc='Move focus to next monitor'),
        Key([mod], "comma", lazy.prev_screen(), desc='Move focus to prev monitor'),

                 # ------------ Hardware Configs ------------
        # Volume
        Key([], "XF86AudioMute", lazy.spawn("amixer set Master toggle"), desc='Mute audio'),
        Key([], "XF86AudioLowerVolume", lazy.spawn(downvol), desc='Volume down'),
        Key([], "XF86AudioRaiseVolume", lazy.spawn(upvol), desc='Volume up'),

        # Brightness
        Key([], "XF86MonBrightnessDown", lazy.spawn(backlight_down), desc='Brightness down'),
        Key([], "XF86MonBrightnessUp", lazy.spawn(backlight_up), desc='Brightness up'),
        ### Switch focus of monitors
        Key([mod], "period", lazy.next_screen(), desc='Move focus to next monitor'),
        Key([mod], "comma", lazy.prev_screen(), desc='Move focus to prev monitor'),
        ### Treetab controls
         Key([mod, "shift"], "h", lazy.layout.move_left(), desc='Move up a section in treetab'),
        Key([mod, "shift"], "l", lazy.layout.move_right(), desc='Move down a section in treetab'),
        ### Window controls
        Key([mod], "j", lazy.layout.down(), desc='Move focus down in current stack pane'),
        Key([mod], "k", lazy.layout.up(), desc='Move focus up in current stack pane'),
        Key([mod, "shift"], "j", lazy.layout.shuffle_down(), lazy.layout.section_down(), desc='Move windows down in current stack'),
        Key([mod, "shift"], "k", lazy.layout.shuffle_up(), lazy.layout.section_up(), desc='Move windows up in current stack'),
        Key([mod], "h", lazy.layout.shrink(), lazy.layout.decrease_nmaster(), desc='Shrink window (MonadTall), decrease number in master pane (Tile)'),
        Key([mod], "l", lazy.layout.grow(), lazy.layout.increase_nmaster(), desc='Expand window (MonadTall), increase number in master pane (Tile)'),
        Key([mod], "n", lazy.layout.normalize(), desc='normalize window size ratios'),
        #Key([mod], "m", lazy.window.toggle_maximize(), desc='Toggle maximize'),
        Key([mod], "m", lazy.function(toggle_max_layout), desc='Toggle maximize'),
        Key([mod, "shift"], "f", lazy.window.toggle_floating(), desc='toggle floating'),
        Key([mod], "f", lazy.window.toggle_fullscreen(), desc='toggle fullscreen'),
        ### Stack controls
        Key([mod, "shift"], "Tab", lazy.layout.rotate(), lazy.layout.flip(), desc='Switch which side main pane occupies (XmonadTall)'),
        Key([mod], "space", lazy.layout.swap_main(), desc='Move window to master'),
        Key([mod, "shift"], "space", lazy.widget["keyboardlayout"].next_keyboard(), desc='Toggle Keyboard Layout'),
        #Key([mod, "shift"], "space",
        #    lazy.layout.toggle_split(),
        #    desc='Toggle between split and unsplit sides of stack'
        #    ),
        Key([mod], "slash", toggle_sticky_windows(), desc="Pin Focused Window",),
        # Keybinds for switching to TTYs on Wayland
        Key(["mod1", "control"], "F1", lazy.core.change_vt(1)),
        Key(["mod1", "control"], "F2", lazy.core.change_vt(2)),
        Key(["mod1", "control"], "F3", lazy.core.change_vt(3)),
        Key(["mod1", "control"], "F4", lazy.core.change_vt(4)),
        Key(["mod1", "control"], "F5", lazy.core.change_vt(5)),
        Key(["mod1", "control"], "F6", lazy.core.change_vt(6)),
        Key(["mod1", "control"], "F7", lazy.core.change_vt(7)),
        # Key Chords List
        # Key Chords are Aborted using <escape>
        # Emacs Key Chords by DistroTube (dwt1) 
        KeyChord([mod],"e", [
                Key([], "e", lazy.spawn(myEmacs), desc='Emacs Dashboard'),
                Key([], "a", lazy.spawn(myEmacs + "--eval '(emms-play-directory-tree \"~/Music/\")'"), desc='Emacs EMMS'),
                Key([], "b", lazy.spawn(myEmacs + "--eval '(ibuffer)'"), desc='Emacs Ibuffer'),
                Key([], "d", lazy.spawn(myEmacs + "--eval '(dired nil)'"), desc='Emacs Dired'),
                Key([], "f", lazy.spawn(myEmacs + "--eval '(elfeed)'"), desc='Emacs Elfeed'),
                Key([], "s", lazy.spawn(myEmacs + "--eval '(eshell)'"), desc='Emacs Eshell'),
                Key([], "v", lazy.spawn(myEmacs + "--eval '(vterm)'"), desc='Emacs Vterm'),
                Key([], "F4", lazy.spawn("killall emacs"),
                        lazy.spawn("/usr/bin/emacs --daemon"),
                        desc='Kill/restart the Emacs daemon')
            ]),
]

groups = [Group(i) for i in "123456789"]

for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + letter of group = switch to & move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(i.name),
            ),
            # Or, use below if you prefer not to switch to that group.
            # # mod1 + shift + letter of group = move focused window to group
            # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            #     desc="move focused window to group {}".format(i.name)),
        ]
    )

import math
from libqtile.utils import rgb

def rounded_corners(colour="00f"):
    def _rounded_corners(ctx, bw, width, height):
        
        bw *=2.0

        # Define border offsets
        x = bw 
        y = bw 

        # Define the radius
        radius = bw / 2
        degrees = math.pi / 180.0

        ctx.new_sub_path()
        
        # Top-right corner
        ctx.arc(width - x, y, radius, -90 * degrees, 0 * degrees)
        
        # Bottom-right corner
        ctx.arc(width - x, height - y, radius, 0 * degrees, 90 * degrees)
        
        # Bottom-left corner
        ctx.arc(x, height - y, radius, 90 * degrees, 180 * degrees)
        
        # Top-left corner
        ctx.arc(x, y, radius, 180 * degrees, 270 * degrees)

        ctx.close_path()

        ctx.set_line_width(bw)
        ctx.set_source_rgba(*rgb(colour))
        ctx.stroke()

    return _rounded_corners

layout_theme = {"border_width": 2,
                "margin": 7,
                "border_focus": "#51afef",
                "border_normal": "#467b96"
                }

layouts = [
    #layout.MonadTall(margin=7, border_width=2, border_focus="#467b96", border_normal="#889fa7", new_client_position="bottom"),
    layout.MonadTall(margin=7, border_width=3,  border_focus=CustomBorder(func=rounded_corners(colour="#467b96")), border_normal=CustomBorder(func=rounded_corners(colour="#889fa7")), new_client_position="bottom"),
    #layout.MonadTall(margin=7, border_width=8,  border_focus=RoundedCorners(colour="#467b96"), border_normal="#889fa7", new_client_position="bottom"),
    #layout.MonadTall(margin=7, border_width=2,  border_focus=GradientBorder(colours=["00f", "0ff"]), border_normal="#889fa7", new_client_position="bottom"),
    #layout.MonadWide(**layout_theme),
    #layout.Bsp(**layout_theme),
    #layout.Stack(stacks=2, **layout_theme),
    #layout.Columns(**layout_theme),
    #layout.RatioTile(**layout_theme),
    #layout.Tile(shift_windows=True, **layout_theme),
    #layout.VerticalTile(**layout_theme),
    #layout.Matrix(**layout_theme),
    #layout.Zoomy(**layout_theme),
    #layout.MonadTall(margin=7, border_width=2, border_focus="#467b96", border_normal="#889fa7", new_client_position="bottom"),
    #layout.Max(margin=7, border_width=2, border_focus="#467b96", border_normal="#889fa7"),
    layout.Max(margin=7, border_width=2, border_focus=CustomBorder(func=rounded_corners(colour="#467b96")), border_normal="#889fa7"),
    layout.Floating(margin=7, border_width=2, border_focus=CustomBorder(func=rounded_corners(colour="#467b96")), border_normal="#889fa7"),
    #layout.Floating(margin=7, border_width=2, border_focus="#467b96", border_normal="#889fa7"),
    #layout.Stack(num_stacks=2),
    #layout.RatioTile(**layout_theme),
    
#layout.TreeTab(
    #     font = "Ubuntu",
    #     fontsize = 10,
    #     sections = ["FIRST", "SECOND", "THIRD", "FOURTH"],
    #     section_fontsize = 10,
    #     border_width = 2,
    #     bg_color = "1c1f24",
    #     active_bg = "c678dd",
    #     active_fg = "000000",
    #     inactive_bg = "a9a1e1",
    #     inactive_fg = "1c1f24",
    #     padding_left = 0,
    #     padding_x = 0,
    #     padding_y = 5,
    #     section_top = 10,
    #     section_bottom = 20,
    #     level_shift = 8,
    #     vspace = 3,
    #     panel_width = 200
    #     ),
]

colors = [["#282c34", "#282c34"],
          ["#1c1f24", "#1c1f24"],
          ["#dfdfdf", "#dfdfdf"],
          ["#ff6c6b", "#ff6c6b"],
          ["#98be65", "#98be65"],
          ["#da8548", "#da8548"],
          ["#51afef", "#51afef"],
          ["#889fa7", "#889fa7"],
          ["#467b96", "#467b96"],
          ["#a9a1e1", "#a9a1e1"]]

prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

##### DEFAULT WIDGET SETTINGS #####
widget_defaults = dict(
    font="Ubuntu Bold",
    fontsize = 14,
    padding = 0,
    background=colors[2]
)
extension_defaults = widget_defaults.copy()

# rd = RectDecoration(
#     use_widget_background=True,
#     radius=12,
#     filled=True,
#     colour='#00000000',
#     group=True,
#     clip=True,
#     #padding_x=-5,
# )
# rd_dec = {"decorations": [rd]}

#rd = PowerLineDecoration(path="arrow_right", padding_y=0)
#rd_dec = {"decorations": [rd]}

rd = PowerLineDecoration(path="forward_slash", padding_y=0)
rd_dec = {"decorations": [rd]}

#rd = PowerLineDecoration(path="rounded_right", padding_y=0)
#rd_dec = {"decorations": [rd]}

screens = [
    Screen(
        top=bar.Bar([
              widget.Sep(
                       linewidth = 0,
                       padding = 6,
                       foreground = colors[2],
                       background = colors[0]
                       ),
              widget.Image(
                       filename = "~/.config/qtile/icons/python-white.png",
                       scale = "False",
                       #mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm)}
                       ),
              widget.Sep(
                       linewidth = 0,
                       padding = 6,
                       foreground = colors[2],
                       background = colors[0]
                       ),
              # Separators allow for extra padding around widgets so it looks good
              #widget.Sep(linewidth = 0, padding=9, background = colors[0], **rd_dec),
              widget.GroupBox(
                    #    font = "Sans Bold",
                       font = "Ubuntu Mono Bold",
                       fontsize = 13,
                       markup = True,
                       # margin_y = 4,
                       margin_y = 3,
                       margin_x = 2,
                       padding_y = 3,
                       padding_x = 7,
                       borderwidth = 2,
                       active = colors[2],
                       inactive = '#696969', 
                       rounded = False,
                       highlight_color = colors[1],
                       highlight_method = "line",
                       this_current_screen_border = colors[6],
                       this_screen_border = colors [4],
                       other_current_screen_border = colors[6],
                       other_screen_border = colors[4],
                       foreground = colors[2],
                       background = colors[0],
                       ),
              widget.TextBox(
                       text = '|',
                       font = "Ubuntu Mono",
                       background = colors[0],
                       foreground = '474747',
                       padding = 2,
                       fontsize = 14
                       ),
              widget.CurrentLayoutIcon(
                       custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")],
                       foreground = colors[2],
                       background = colors[0],
                       padding = 0,
                       scale = 0.7
                       ),
              widget.CurrentLayout(
                       foreground = colors[2],
                       background = colors[0],
                       padding = 5
                       ),
              widget.TextBox(
                       text = '|',
                       font = "Ubuntu Mono",
                       background = colors[0],
                       foreground = '474747',
                       padding = 2,
                       fontsize = 14
                       ),
              widget.WindowName(
                       foreground = colors[6],
                       background = colors[0],
                       padding = 8,
                       update_interval = 0.001,
                       fontsize = 15,
                       max_chars=60,
                       width=bar.CALCULATED,
                       empty_group_string = '',
                       **rd_dec
                       ),
              #widget.Cmus(
              #         foreground = colors[6],
              #         background = colors[0],
              #         play_color = colors[6],
              #         noplay_color = colors[7],
              #         padding = 8,
              #         update_interval = 0.001,
              #         fontsize = 15,
              #         max_chars=30,
              #         width=bar.CALCULATED,
              #         format='{play_icon} {title}',
              #         ,**rd_dec
              #         ),
             
              # widget.Spacer( background = '#00000000'),
              widget.Spacer( background = '#282c34'),
              # widget.Sep(linewidth = 0, padding=9, background = colors[0], **rd_dec),

              StatusNotifier(
                       background = colors[0],
                       icon_theme = "Sardi-Flat-Arc",
                       icon_size = 16,
                       # The following options are for the StatusNotfier from qtile-extras
                       highlight_colour = '308dcd', # 308dcd is a darker shade of 51afef that looks closer to the way 51afef is rendered on the WindowName widget
                       menu_background = '282c34',
                       menu_font = 'Ubuntu Bold',
                       menu_fontsize = 11,
                       menu_foreground = 'dfdfdf',
                       # The padding is for either version of the StatusNotifier widget
                       padding = 1,
                       # **rd_dec
                        ),
              widget.Systray(
                       background = colors[0],
                       icon_size = 16,
                       padding = 1,
                       # **rd_dec
                       ),
              widget.KeyboardLayout(
                       background = colors[0],
                       configured_keyboards = ['gb', 'bg phonetic'],
                       display_map = {'gb': 'gb', 'bg phonetic': 'bg'},
                       fmt = 'Kb: {}',
                       padding = 3,
                       # **rd_dec
                       ),

              # widget.Sep(linewidth = 0, padding=9, background = colors[0], **rd_dec),
              # widget.Spacer( background = '#00000000'),
              # widget.Spacer( background = '#282c34'),
              widget.Sep(linewidth = 0, padding=4, background = colors[0], **rd_dec),
              widget.Backlight(
                       foreground = colors[2],
                       background = colors[8],
                       brightness_file = '/sys/class/backlight/amdgpu_bl1/brightness',
                       max_brightness_file = '/sys/class/backlight/amdgpu_bl1/max_brightness',
                       format = ' 󱄄    {percent:2.0%} ', 
                       **rd_dec
                       ),
              # widget.Sep(linewidth = 0, padding=9, background = colors[8], **rd_dec),
              # widget.Sep(linewidth = 0, padding=7, background = '#00000000'),
              # widget.Sep(linewidth = 0, padding=9, background = colors[7], **rd_dec),
              widget.CPU(
                       foreground = colors[0],
                       background = colors[7],
                       padding = 5,
                       format = '    {load_percent}%', 
                       **rd_dec
                       ),
              # widget.Sep(linewidth = 0, padding=9, background = colors[7], **rd_dec),
              # widget.Sep(linewidth = 0, padding=7, background = '#00000000'),
              # widget.Sep(linewidth = 0, padding=9, background = colors[8], **rd_dec),
              widget.Battery(
                       foreground = colors[2],
                       background = colors[8],
                       battery = 1,
                       discharge_char = '',
                       low_foreground = '#ff0000',
                       low_percentage = 0.15,
                       empty_char = 'X',
                       fmt = '    {}',
                       format = '{char} {percent:2.0%}',
                       padding = 5,
                       **rd_dec
                       ),
              # widget.Sep(linewidth = 0, padding=9, background = colors[8], **rd_dec),
              # widget.Sep(linewidth = 0, padding=7, background = '#00000000'),
              # widget.Sep(linewidth = 0, padding=9, background = colors[7], **rd_dec),
              widget.Volume(
                       foreground = colors[0],
                       background = colors[7],
                       fmt = '󰕾   {}',
                       padding = 5,
                       **rd_dec
                       ),
              # widget.Sep(linewidth = 0, padding=9, background = colors[7], **rd_dec),
              # widget.Sep(linewidth = 0, padding=7, background = '#00000000'),
              # widget.Sep(linewidth = 0, padding=9, background = colors[8], **rd_dec),
              widget.ThermalSensor(
                       foreground = colors[2],
                       background = colors[8],
                       padding = 5,
                       threshold = 90,
                       format = '   {temp:.0f}{unit}',
                       **rd_dec
                       ),
              # widget.Sep(linewidth = 0, padding=9, background = colors[8], **rd_dec),
              # widget.Sep(linewidth = 0, padding=7, background = '#00000000'),
              # widget.Sep(linewidth = 0, padding=9, background = colors[7], **rd_dec),
              widget.Memory(
                       foreground = colors[0],
                       background = colors[7],
                       format = '󰒋   {MemUsed:.0f}{mm}/{MemTotal:.0f}{mm}',
                       padding = 5,
                       **rd_dec
                       ),
              #widget.Sep(linewidth = 0, padding=9, background = colors[7], **rd_dec),
              #widget.Sep(linewidth = 0, padding=7, background = '#00000000'),
              #widget.Sep(linewidth = 0, padding=9, background = colors[8], **rd_dec),
              widget.Clock(
                       foreground = colors[2],
                       background = colors[8],
                       format = " 󰃭    %A, %d %B - %H:%M ",
                       ),
              # widget.Sep(linewidth = 0, padding=9, background = colors[8], **rd_dec),
              # widget.Sep(
                        # linewidth = 0,
                        # foreground = '#00000000',
                        # background = '#00000000',
                        # padding = 4),
            ],
            24,
            background="#282c34",
            #background="00000000",
            #margin = [5, 5, 5, 5],
        ),
    )
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)

def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)

def window_to_previous_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group)

def window_to_next_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group)

def switch_screens(qtile):
    i = qtile.screens.index(qtile.current_screen)
    group = qtile.screens[i - 1].group
    qtile.current_screen.set_group(group)

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating( margin=7, border_width=2, border_focus=CustomBorder(func=rounded_corners(colour="#467b96")), border_normal="#889fa7",
#layout.Floating( margin=7, border_width=2, border_focus="#467b96", border_normal="#889fa7",
#layout.Floating( margin=7, border_width=2, border_focus=CustomBorder(func=rounded_corners(colour="#467b96")), border_normal="#889fa7",
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
	Match(title='Bluetooth'),         # Blueberry
	Match(title='Bluetooth Devices'),         # Blueman
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# Function to bring all floating windows to the front
def bring_floats_to_front():
    for window in qtile.current_group.windows:
        if window.floating:
            window.cmd_bring_to_front()

# Hook to bring floating windows to the front when a new window is created
@hook.subscribe.client_new
def on_new_client(window):
    bring_floats_to_front()

# Hook to bring floating windows to the front when a window is managed
@hook.subscribe.client_managed
def on_client_managed(window):
    bring_floats_to_front()

# Hook to bring floating windows to the front when a window's floating state changes
@hook.subscribe.float_change
def on_float_change(window):
    bring_floats_to_front()

auto_minimize = True

@hook.subscribe.startup_once
def start_once():
    if qtile.core.name == "x11":
        # subprocess.call([home + '/.config/qtile/scripts/autostart-theme1.sh'])
        subprocess.Popen([home + '/.config/qtile/scripts/autostart-theme1.sh'])
        lazy.screen.set_wallpaper([home + '/.config/qtile/wall.jpg'], mode='fill')
    elif qtile.core.name == "wayland":
        subprocess.Popen([home + '/.config/qtile/scripts/autostart-theme1-wayland.sh']),
        subprocess.Popen(lazy.reload_config()),
        lazy.screen.set_wallpaper([home + '/.config/qtile/wall.jpg'], mode='fill')

wl_input_rules = {
   "1267:12477:ELAN1300:00 04F3:3047 Touchpad": InputConfig(left_handed=False, tap = True),
   "*": InputConfig(left_handed=False, pointer_accel=False, tap = True),
   "type:pointer": InputConfig(pointer_accel=-1.0),
   "type:keyboard": InputConfig(kb_layout="gb"),
}

lazy.spawn("wlr-randr --output X11-1 --on --custom-mode 1920x1080@60Hz")
lazy.reload_config(),

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
