#!/bin/bash

cp $HOME/.config/qtile/config-theme1.py $HOME/.config/qtile/config.py

cp $HOME/.config/starship-theme1.toml $HOME/.config/starship.toml

cp $HOME/.config/fish/config-theme1.fish $HOME/.config/fish/config.fish
