#!/usr/bin/env python3
import os
# !!!
# !!! Define Variables
# !!!

distro = 0
setup = 0
distrolist = [" ", "Arch", "NixOS", "Fedora", "OpenSUSE TW"] # Empty first choice to cover for index 0 in the array
# Currently unused because it's a WIP
setuplist = [" ", "Hyprland", "River", "AwesomeWM", "Qtile", "Sway", "BSPWM", "XMonad", "DWM", "All"] # Empty first choice to cover for index 0 in the array

print(chr(57))

# Old Arch Pkglist, Contains everything
#arch_main_pkglist = "kitty awesome conky htop neofetch feh rofi dmenu thunar qtile brightnessctl python-pywayland python-pywlroots xorg-xwayland lxappearance nwg-look codium ttf-ubuntu-font-family ttf-hack xfce4-screenshooter blueman codium discord powertop gvfs wayshot grim grimshot wl-clipboard xclip sddm vlc android-file-transfer samba libwbclient bemenu wofi nitroshare meld qbittorrent cups xorg-xlsclients hyprland bc man-db xorg-xsetroot swaybg wallutils bspwm polybar sxhkd thunar-volman thunar-archive-plugin xmonad xmonad-contrib xmobar qt5-quickcontrols2 qt5-graphicaleffects brave-bin cmus mypy neovim vim alsa-utils bash cups dunst networkmanager networkmanager-applet shell-color-scripts xorg-server xf86-video-amdgpu xorg-xsetroot xterm zsh"
# New Arch Pkglist, contains only Hyprland
arch_main_pkglist = "foot htop neofetch thunar qtile qtile-extras brightnessctl python-pywayland python-pywlroots xorg-xwayland nwg-look ttf-ubuntu-font-family ttf-hack bluez-utils powertop gvfs grim grimshot wl-clipboard sddm android-file-transfer bemenu nitroshare meld qbittorrent cups xorg-xlsclients hyprland bc man-db swaybg wallutils thunar-volman thunar-archive-plugin qt5-quickcontrols2 qt5-graphicaleffects cmus mypy neovim vim alsa-utils bash dunst networkmanager networkmanager-applet xf86-video-amdgpu zsh"
fedora_main_pkglist = ""
suse_tw_main_pkglist = ""
flatpak_pkglist = "discord-screenaudio"
# Hyprland_pkglist = "hyprland waybar"
# River_pkglist = "river waybar"
# AwesomeWM_pkglist = "awesome"
# !!!
# !!! Define Functions
# !!!
print ("WARNING: Only Arch and Hyprland are supported currently; no other choices will work!")
def distrosel(distro):
    # The following while loop ensures that it is continued while the character is before 1 (ASCII 49) and after 4 (ASCII 52), the loop will continue and the program will not leave it.  
    # CORRECTION: For the time being, we're using 1 (ASCII 49) only.
    while ord(str(distro)) < 49 or ord(str(distro)) > 49:
        print("Distro choices: \n 1. Arch \n 2. NixOS \n 3. Fedora \n 4. OpenSUSE Tumbleweed")
        distro = input("Select distro number: ")
        try:
            int(distro)
        except:
            print ("Error: Please use a whole number instead")
            distro = 0
        try:
            ord(str(distro))
        except:
            print ("Error: Please only type in a single character")
            distro = 0
        
    
    return distro

# This is the first part of the setup selection. It is currently disabled, because the scope of the setup script is becoming a bit too overwhelming. 
# Currently, the script will just setup all possible window managers for the chosen distro.
# def setupsel(setup):
#     # The following while loop ensures that it is continued while the character is before 1 (ASCII 49) and after 4 (ASCII 52), the loop will continue and the program will not leave it.  
#     while ord(str(setup)) < 49 or ord(str(setup)) > 57:
#         print("Distro choices: \n 1. Hyprland \n 2. River \n 3. AwesomeWM \n 4. Qtile \n 5. Sway \n 6. BSPWM \n 7. XMonad \n 8. DWM \n 9. All")
#         setup = input("Select setup number: ")
#         try:
#             int(setup)
#         except:
#             print ("Error: Please use a whole number instead")
#             setup = 0
#         try:
#             ord(str(setup))
#         except:
#             print ("Error: Please only type in a single character")
#             setup = 0

#     return setup

def installchoice(distro):
    if distro == "Arch":
        try:
            os.system("sudo pacman -S git ")
            os.system("sudo pacman -S --needed base-devel")
            os.system("git clone https://aur.archlinux.org/paru.git && cd paru && makepkg -si && cd")
            os.system("mkdir .temp && cd .temp")
            dots_setup()
            os.system("cd .. && rm -rf .temp")
            os.system(f"paru -S {arch_main_pkglist}")
        except:
            status = "Error"
        else: 
            status = "Success"
    elif distro == "NixOS":
        try:
            print("nixos")
        except:
            status = "Error"
        else: 
            status = "Success"
    elif distro == "Fedora":
        try:
            print("fedora")
        except:
            status = "Error"
        else: 
            status = "Success"
    elif distro == "OpenSUSE TW":
        try:
            print("tumbleweed")
        except:
            status = "Error"
        else: 
            status = "Success"
    else:
        status = "ChoiceError"
    
    return status

def dots_setup():
    # Set up .dots-tmp dir to remove setup directories after setup and leave $HOME clean.

            os.system("mkdir .temp && cd .temp")
    # Clone Repos
    os.system("git clone https://gitlab.com/theshatterstone/dotfiles")
    os.system("git clone https://gitlab.com/theshatterstone/neovim")
    os.system("git clone https://gitlab.com/theshatterstone/linux-themes")
    os.system("git clone https://gitlab.com/theshatterstone/sddm")
    os.system("git clone https://gitlab.com/theshatterstone/dwm")
    # Set Up Dots
    os.system("cp -r dotfiles/.config $HOME/")
    os.system("cp dotfiles/.bashrc $HOME/")
    os.system("cp -r neovim/.config $HOME/")
    os.system("cp -r neovim/local $HOME/")
    os.system("cp -r linux-themes/.themes $HOME/")
    os.system("cp -r linux-themes/.icons $HOME/")
    # Set Up zsh config (WIP)
    os.system("cp dotfiles/.zshrc ~/")
    # os.system("cp -r dotfiles-fresh/.oh-my-zsh ~/")
    os.system("sudo mkdir -p /etc/sddm.conf.d")
    os.system("sudo mkdir -p /usr/share/sddm/themes")
    os.system("sudo cp sddm/etc/sddm.conf.d/sddm.conf /etc/sddm.conf.d/")
    os.system("sudo cp -r sddm/usr/share/sddm/themes/sugar-dark /usr/share/sddm/themes")




# !!!
# !!! Main Program Flow
# !!!

distronum = int(distrosel(distro))
if distronum != 1:
    print("Error: Only Arch is currently supported. Other distros are a work in progress.")
    quit()

distro = distrolist[distronum]
print (distro)

# setupnum = int(setupsel(setup))
# setup = setuplist[setupnum]
# if setupnum != 1:
#     print("Error: Only Hyprland is currently supported. Other setups are a work in progress.")
#     quit()


ufetch = input("Would you like to install ufetch (Yy/Nn): ")
while ufetch != "y" and ufetch != "n" and ufetch != "Y" and ufetch != "N":
    ufetch = input("Would you like to install ufetch (Yy/Nn): ")
if ufetch == "y" or ufetch == "Y": 
    ufetch = input("Would you like to install ufetch Globally or locally (Gg/Ll): ")
    while ufetch != "g" and ufetch != "l" and ufetch != "G" and ufetch != "L":
        ufetch = input("Would you like to install ufetch Globally or locally (Gg/Ll): ")
    if ufetch == "g" or ufetch == "G":
        if distro == "Arch":
            os.system("sudo curl https://gitlab.com/jschx/ufetch/-/raw/main/ufetch-arch -o /usr/bin/ufetch && sudo chmod +x /usr/bin/ufetch") 
        elif distro == "NixOS":
            print("Global install not supported. Falling back to local install")
            ufetch = "L"
        elif distro == "Fedora":
            os.system("sudo curl https://gitlab.com/jschx/ufetch/-/raw/main/ufetch-fedora -o /usr/bin/ufetch && sudo chmod +x /usr/bin/ufetch") 
        elif distro == "OpenSUSE TW":
            os.system("sudo curl https://gitlab.com/jschx/ufetch/-/raw/main/ufetch-suse -o /usr/bin/ufetch && sudo chmod +x /usr/bin/ufetch") 
    elif ufetch == "l" or ufetch == "L":
        if distro == "Arch":
            os.system("mkdir -p $HOME/.local/bin")
            os.system("curl https://gitlab.com/jschx/ufetch/-/raw/main/ufetch-arch -o $HOME/.local/bin/ufetch && chmod +x $HOME/.local/bin/ufetch")
        elif distro == "NixOS":
            os.system("mkdir -p $HOME/.local/bin")
            os.system("curl https://gitlab.com/jschx/ufetch/-/raw/main/ufetch-nixos -o $HOME/.local/bin/ufetch && chmod +x $HOME/.local/bin/ufetch")
        elif distro == "Fedora":
            os.system("mkdir -p $HOME/.local/bin")
            os.system("curl https://gitlab.com/jschx/ufetch/-/raw/main/ufetch-fedora -o $HOME/.local/bin/ufetch && chmod +x $HOME/.local/bin/ufetch") 
        elif distro == "OpenSUSE TW":
            os.system("mkdir -p $HOME/.local/bin")
            os.system("curl https://gitlab.com/jschx/ufetch/-/raw/main/ufetch-suse -o $HOME/.local/bin/ufetch && chmod +x $HOME/.local/bin/ufetch")
else:
    print("Ufetch installation skipped")


vim_plug = input("Would you like to install vim-plug (Yy/Nn): ")
while vim_plug != "y" and vim_plug != "n" and vim_plug != "Y" and vim_plug != "N":
    vim_plug = input("Would you like to install vim_plug (Yy/Nn): ")
if vim_plug == "y" or vim_plug == "Y": 
#     os.system(sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim')
    print("Installing vim-plug...")
else:
    print("Vim-Plug installation skipped")

starship = input("Would you like to install starship prompt (Yy/Nn): ")
while starship != "y" and starship != "n" and starship != "Y" and starship != "N":
    starship = input("Would you like to install starship (Yy/Nn): ")
if starship == "y" or starship == "Y": 
    os.system("curl -sS https://starship.rs/install.sh | sh")
else:
    print("Starship prompt installation skipped")

if distro != "NixOS":
    bsp_layout = input("Would you like to install bsp-layout (Yy/Nn): ")
    while bsp_layout != "y" and bsp_layout != "n" and bsp_layout != "Y" and bsp_layout != "N":
        bsp_layout = input("Would you like to install bsp-layout (Yy/Nn): ")
    if bsp_layout == "y" or bsp_layout == "Y": 
        os.system("curl https://raw.githubusercontent.com/phenax/bsp-layout/master/install.sh | bash")
    else:
        print("bsp-layout installation skipped")

installstatus = installchoice(distro)
if installstatus == "Success":
    print("Setup Completed with no errors")
else:
    print("Setup Error:")
    if installstatus == "ChoiceError":
        print("Choice Error: Choice could not be detected")
    else:
        print("Unknown Error")
# print(f"The chosen setup is {setup} on {distro}")

